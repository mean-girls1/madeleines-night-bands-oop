import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Service service = new Service();
        Scanner scanner = new Scanner(System.in);
        String choice;
        boolean isRunning = true;

        service.setUp();

        while (isRunning) {
            System.out.println("\nMake your choice: ");
            choice = scanner.next();
            switch (choice) {
                case "1" -> {
                    System.out.println("Listing all bands and musicians\n");
                    service.printAllBands();
                }
                case "2" -> {
                    System.out.println("Play one night\n");
                    service.playOneNight();
                }
                case "3" -> {
                    System.out.println("Play swapping instruments\n");
                    service.jamSession();
                }
                case "4" -> {
                    System.out.println("Exiting Life of Crazy Musicians");
                    isRunning = false;
                }
                default -> {
                    System.out.println("Invalid choice. Try typing a number between 1-4 :) ");
                    service.printMenu();
                }
            }
        }
    }
}
