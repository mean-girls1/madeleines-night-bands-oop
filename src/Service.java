import utils.Band;
import utils.Data;


public class Service {

    PlayOneNight playOneNight = new PlayOneNight();
    JamSession jamSession = new JamSession();

    public void setUp() {
        Data.setUpData();
        printWelcomeText();
        printMenu();
    }

    private void printWelcomeText() {
        System.out.println("\nWelcome to Life of Crazy Musicians!");
    }

    public void printMenu() {
        System.out.println("\nChoose action from the menu: ");
        System.out.println("1: Show all bands and musicians");
        System.out.println("2: Let the musicians play one night and see " +
                "\n   what musicians will get angry and leave their bands");
        System.out.println("3: Let the bandless musicians " +
                "\n   play the swapping game with their instruments");
        System.out.println("4: Exit Life of Crazy Musicians");
    }

    public void printAllBands() {
        Data.getBands().forEach(Band::printBandInfo);
        System.out.println("Bandless Musicians: ");
        Data.getBandlessMusicians().forEach(musician ->
                System.out.println(musician.getMusicianInfo()));
    }

    public void playOneNight() {
        playOneNight.mixMusicians();
    }

    public void jamSession() {
        jamSession.mixInstruments();
    }
}
