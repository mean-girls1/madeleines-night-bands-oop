package utils.intruments;

import utils.intruments.Instrument;


public abstract class Keyboard extends Instrument {
    private final int numberOfKeys;
    private final String feature;

    public Keyboard(String manufacturer, int numberOfKeys, String feature) {
        super(manufacturer);
        this.numberOfKeys = numberOfKeys;
        this.feature = feature;
    }

    public abstract String getInstrumentName();

    public String infoToString() {
        return manufacturerToString() +
                "\n  - Number of keys: " + numberOfKeys +
                "\n  - Feature: " + feature;
    }
}
