package utils.intruments;


public class Synthesizer extends Keyboard {

    public Synthesizer(String manufacturer, int numberOfKeys, String feature) {
        super(manufacturer, numberOfKeys, feature);
    }

    @Override
    public String getInstrumentName() {
        return "Synthesizer";
    }

    @Override
    public String printInfo() {
        return "\n - Instrument: " + getInstrumentName() +
                infoToString();
    }
}
