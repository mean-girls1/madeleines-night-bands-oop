package utils.intruments;

import utils.intruments.Instrument;


public abstract class StringInstrument extends Instrument {
    final int numberOfStrings;
    final double stringSize;

    public StringInstrument(String manufacturer, int numberOfStrings, double stringSize) {
        super(manufacturer);
        this.numberOfStrings = numberOfStrings;
        this.stringSize = stringSize;
    }

    public abstract String getInstrumentName();

    public String infoToString() {
        return manufacturerToString() +
                "\n  - Number of strings: " + numberOfStrings +
                "\n  - String size: " + stringSize + "mm";
    }
}
