package utils.intruments;

import utils.intruments.Instrument;


public abstract class PercussionInstrument extends Instrument {
    private final boolean hiHat;

    public PercussionInstrument(String manufacturer, boolean hiHat) {
        super(manufacturer);
        this.hiHat = hiHat;
    }

    public abstract String getInstrumentName();

    public String infoToString() {
        return manufacturerToString() +
                "\n  - Feature: " + (hiHat ? "has hi-hat" : "no hi-hat");
    }
}
