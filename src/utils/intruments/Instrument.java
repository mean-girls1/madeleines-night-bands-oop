package utils.intruments;

public abstract class Instrument {
    String manufacturer;

    public Instrument(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public abstract String printInfo();

    public String manufacturerToString() {
        return "\n  - Manufacturer: " + manufacturer;
    }
}
