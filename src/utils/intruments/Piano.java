package utils.intruments;


public class Piano extends Keyboard {

    public Piano(String manufacturer, int numberOfKeys, String feature) {
        super(manufacturer, numberOfKeys, feature);
    }

    @Override
    public String getInstrumentName() {
        return "Piano";
    }

    @Override
    public String printInfo() {
        return "\n - Instrument: " + getInstrumentName() +
                infoToString();
    }
}
