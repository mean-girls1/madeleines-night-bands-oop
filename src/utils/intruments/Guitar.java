package utils.intruments;


public class Guitar extends StringInstrument {

    public Guitar(String manufacturer, int numberOfStrings, double stringSize) {
        super(manufacturer, numberOfStrings, stringSize);
    }

    @Override
    public String getInstrumentName() {
        return "Guitar";
    }

    @Override
    public String printInfo() {
        return "\n - Instrument: " + getInstrumentName() +
                infoToString();
    }
}
