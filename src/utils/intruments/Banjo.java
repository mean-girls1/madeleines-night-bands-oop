package utils.intruments;


public class Banjo extends StringInstrument {

    public Banjo(String manufacturer, int numberOfStrings, double stringSize) {
        super(manufacturer, numberOfStrings, stringSize);
    }

    @Override
    public String getInstrumentName() {
        return "Banjo";
    }

    @Override
    public String printInfo() {
        return "\n - Instrument: " + getInstrumentName() +
                infoToString();
    }
}
