package utils.intruments;


public class Drums extends PercussionInstrument {

    public Drums(String manufacturer, boolean hiHat) {
        super(manufacturer, hiHat);
    }

    @Override
    public String getInstrumentName() {
        return "Drums";
    }

    @Override
    public String printInfo() {
        return "\n - Instrument: " + getInstrumentName() +
                infoToString();
    }
}
