package utils;

import java.util.ArrayList;
import java.util.stream.Collectors;


public class Band {
    private final String name;
    private final ArrayList<Musician> musicians;

    public Band(String name, ArrayList<Musician> musicians) {
        this.name = name;
        this.musicians = musicians;
    }

    public void printBandInfo() {
        System.out.println("Band: " + name +
                "\n Musicians: " + getMusicians().stream()
                .map(Musician::getMusicianInfo)
                .collect(Collectors.joining("\n", "\n", "\n")));
    }

    public boolean addsMusician(Band band, Musician musician) {
        if (band.getMusicians().stream().noneMatch(musician1 ->
                musician1.getInstrument().getClass().getSimpleName()
                        .equals(musician.getInstrument().getClass().getSimpleName()))) {
            musicians.add(musician);
            return true;
        }
        return false;
    }

    public boolean removeMusician(Musician musician) {
        if (this.musicians.size() > 0) {
            this.musicians.remove(musician);
            return true;
        }
        return false;
    }

    public ArrayList<Musician> getMusicians() {
        return musicians;
    }

    public String getName() {
        return name;
    }
}