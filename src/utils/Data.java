package utils;

import utils.intruments.Banjo;
import utils.intruments.Drums;
import utils.intruments.Guitar;
import utils.intruments.Piano;
import utils.intruments.Synthesizer;

import java.util.ArrayList;


public class Data {

    static ArrayList<Band> bands = new ArrayList<>();
    static ArrayList<Musician> bandlessMusicians = new ArrayList<>();

    static Piano mcMiren = new Piano("McMiren", 81, "is acoustic");
    static Synthesizer arc = new Synthesizer("Arc", 84, "has LED");
    static Drums toyota = new Drums("Toyota", true);
    static Guitar minder = new Guitar("Minder", 8, .4);
    static Guitar screen22 = new Guitar("Screen22", 3, 8);
    static Banjo fibson = new Banjo("Fibson", 4, 8);
    static Synthesizer suzuki = new Synthesizer("Suzuki", 72, "has no LED");

    static Musician emend = new Musician("Ilea Emend", minder);
    static Musician angel = new Musician("Mr Angel", mcMiren);
    static Musician leghorn = new Musician("Jut Leghorn", arc);
    static Musician one = new Musician("Mr One", fibson);
    static Musician inks = new Musician("A Gaze Inks", toyota);
    static Musician no = new Musician("Lacier No", suzuki);
    static Musician meed = new Musician("Alien Meed", screen22);

    static Band adrenalEx = new Band("Adrenal Ex", new ArrayList<>());
    static Band idSeen = new Band("I'd Seen", new ArrayList<>());

    public static void setUpData() {
        addBandsToList();
        addMusiciansToBands();
        addBandlessMusiciansToList();
    }

    private static void addBandsToList() {
        bands.add(adrenalEx);
        bands.add(idSeen);
    }

    private static void addMusiciansToBands() {
        adrenalEx.addsMusician(adrenalEx, emend);
        adrenalEx.addsMusician(adrenalEx, angel);
        adrenalEx.addsMusician(adrenalEx, leghorn);
        idSeen.addsMusician(idSeen, one);
        idSeen.addsMusician(idSeen, inks);
        idSeen.addsMusician(idSeen, no);
    }

    private static void addBandlessMusiciansToList() {
        bandlessMusicians.add(meed);
    }

    public static ArrayList<Band> getBands() {
        return bands;
    }

    public static ArrayList<Musician> getBandlessMusicians() {
        return bandlessMusicians;
    }
}
