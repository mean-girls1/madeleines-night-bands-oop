package utils;


import utils.intruments.Instrument;


public class Musician {
    private final String name;
    private Instrument instrument;

    public Musician(String name, Instrument instrument) {
        this.name = name;
        this.instrument = instrument;
    }

    public String getMusicianInfo() {
        if (instrument != null) {
            return " Name: " + name + instrument.printInfo();
        } else
            return " Name: " + name + "\n Instrument: none";
    }

    public void removeInstrument(Musician musician) {
        musician.setInstrument(null);
    }

    public Instrument getInstrument() {
        return instrument;
    }

    public void setInstrument(Instrument instrument) {
        if (this.instrument == null) {
            this.instrument = instrument;
        }
    }

    public String getName() {
        return name;
    }
}
