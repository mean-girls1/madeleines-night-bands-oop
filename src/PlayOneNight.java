import utils.Band;
import utils.Data;
import utils.Musician;

import java.util.Random;


public class PlayOneNight {

    public void mixMusicians() {
        for (int i = 0; i < Data.getBands().size(); i++) {
            Band band = Data.getBands().get(i);
            removeRandomMusician(band);
            addBandlessMusicianToRandomBand();
        }
    }

    public void removeRandomMusician(Band band) {
        Musician randomMusician = getRandomMusician(band);
        if (band.removeMusician(randomMusician) && randomMusician != null) {
            Data.getBandlessMusicians().add(randomMusician);
            System.out.println("Musician " + randomMusician.getName() + " left " + band.getName());
        }
    }

    private void addBandlessMusicianToRandomBand() {
        Musician bandlessMusician = getRandomBandlessMusician();
        Band randBand = getRandomBand();
        if (bandlessMusician != null && randBand != null) {
            addBandlessMusicianToBand(randBand, bandlessMusician);
        }
    }

    private void addBandlessMusicianToBand(Band randBand, Musician bandlessMusician) {
        if (randBand.addsMusician(randBand, bandlessMusician)) {
            Data.getBandlessMusicians().remove(bandlessMusician);
            System.out.println("Musician " + bandlessMusician.getName() + " joined " + randBand.getName());
        }
    }

    private Band getRandomBand() {
        if (Data.getBands().size() > 0) {
            Random rand = new Random();
            return Data.getBands().get(rand.nextInt(Data.getBands().size()));
        }
        return null;
    }

    private Musician getRandomMusician(Band band) {
        if (band.getMusicians().size() > 0) {
            Random rand = new Random();
            return band.getMusicians().get(rand.nextInt(band.getMusicians().size()));
        }
        return null;
    }

    private Musician getRandomBandlessMusician() {
        if (Data.getBandlessMusicians().size() > 0) {
            Random rand = new Random();
            return Data.getBandlessMusicians().get(rand.nextInt(Data.getBandlessMusicians().size()));
        }
        return null;
    }
}
