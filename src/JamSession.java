import utils.Musician;
import utils.Data;
import utils.intruments.Instrument;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class JamSession {

    public void mixInstruments() {
        Random rand = new Random();
        List<Instrument> instruments = new ArrayList<>();
        if(Data.getBandlessMusicians().size() > 0) {
            for (Musician musician : Data.getBandlessMusicians()) {
                addInstrumentToListAndRemoveFromMusician(instruments, musician);
            }
            for (Musician musician : Data.getBandlessMusicians()) {
                addRandomInstrumentToMusician(rand, instruments, musician);
            }
        } else System.out.println("Nobody is here to play :(");
    }

    private void addRandomInstrumentToMusician(Random rand, List<Instrument> instruments, Musician musician) {
        Instrument randInstrument = instruments.get(rand.nextInt(instruments.size()));
        musician.setInstrument(randInstrument);
        System.out.println(musician.getName() + " got a " + musician.getInstrument().getClass().getSimpleName());
        instruments.remove(randInstrument);
    }

    private void addInstrumentToListAndRemoveFromMusician(List<Instrument> instruments, Musician musician) {
        instruments.add(musician.getInstrument());
        System.out.println(musician.getName() + " puts their " +
                musician.getInstrument().getClass().getSimpleName() + " in the mix pile");
        musician.removeInstrument(musician);
    }
}
